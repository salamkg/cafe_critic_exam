<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cafe;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadCafe implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setName('Restaurants');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setName('Cafes');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setName('Pubs');
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setName('Taverns');
        $manager->persist($category4);

        $manager->flush();
    }
}