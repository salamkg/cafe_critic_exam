<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Cafe", mappedBy="category")
     */
    private $cafes;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cafes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cafe
     *
     * @param \AppBundle\Entity\Cafe $cafe
     *
     * @return Category
     */
    public function addCafe(\AppBundle\Entity\Cafe $cafe)
    {
        $this->cafes[] = $cafe;

        return $this;
    }

    /**
     * Remove cafe
     *
     * @param \AppBundle\Entity\Cafe $cafe
     */
    public function removeCafe(\AppBundle\Entity\Cafe $cafe)
    {
        $this->cafes->removeElement($cafe);
    }

    /**
     * Get cafes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCafes()
    {
        return $this->cafes;
    }

    public function __toString()
    {
        return $this->name ?: '';
    }
}
