<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cafe;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Picture;
use AppBundle\Form\CafeType;
use AppBundle\Form\CommentType;
use AppBundle\Form\PictureType;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $cafes = $this->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $cafes,
            $request->query->getInt('page', 1)/*page number*/,
            20/*limit per page*/
        );

        return $this->render('AppBundle:Main:index.html.twig', array(
            'cafes' => $cafes,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/new-cafe")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $cafe = new Cafe();
        $form = $this->createForm(CafeType::class, $cafe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $cafe->setUser($this->getUser());
            $cafe = $form->getData();
            // $file - объект класса для управления скачанным файлом
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $cafe->getPicture();
            // Генерируем уникальное имя файла, прежде чем сохраним его в папку с остальными файлами
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Перемещаем файл в папку с изображениями всех продуктов.
            $file->move(
                $this->getParameter('cafe_image_directory'),
                $fileName
            );
            // Сохраняем итоговое имя файла в поле сущности
            $cafe->setPicture($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($cafe);
            $em->flush();

            return $this->redirectToRoute('app_main_index', array(
                'id' => $cafe->getId(),
            ));
        }

        return $this->render('@App/Main/new-cafe.html.twig', array(
            'form' => $form->createView(),
            'cafe' => $cafe,
        ));
    }

    /**
     * @Route("/cafe/{id}", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cafePageAction(int $id, Request $request)
    {
        $cafe_info = $this->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->find($id);

        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $comment->setCafe($cafe_info);
            $comment->setUser($this->getUser());
            $comment = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('app_main_cafepage', array(
                'id' => $id,
            ));
        }
        return $this->render('@App/Main/cafe-info.html.twig', array(
            'cafe_info' => $cafe_info,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/addPicture/{id}", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPictureAction(Request $request, int $id)
    {
        $cafe = $this->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->find($id);
        $picture = new Picture();
        $form = $this->createForm(PictureType::class, $picture);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $picture->setUser($this->getUser());
            $picture->setCafe($cafe);
            $picture = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($picture);
            $em->flush();

            return $this->redirectToRoute("app_main_cafepage", [
                'id' => $id,
            ]);
        }

        return $this->render('@App/Main/new-picture.html.twig', array(
            'cafe' => $cafe,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findDataAction(Request $request)
    {
        $data = $request->request->get(trim('data'));
        $finder = $this->container->get('fos_elastica.finder.app.cafe');

        $result = $finder->find($data);

        $record = $this->getDoctrine()->getRepository('AppBundle:Cafe')
            ->findCafe($data);


        return $this->render('@App/Main/search-result.html.twig', array(
            'record' => $record,
            'result' => $result,
            'data' => $data,
        ));
    }
}
